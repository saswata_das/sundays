import 'package:flutter/material.dart';
import 'dart:io';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';

import '../modules/fetch_weather.dart';

class AppSplash extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    void onFetchChangeScreen() async {
      var fetchStatus = await fetchData();
      sleep(const Duration(milliseconds: 1500));
      if (fetchStatus == true) {
        Navigator.pushReplacementNamed(context, '/overview');
      } else {
        FlutterToast(context).showToast(
            child: Text('Sorry, we are facing some unwanted issues.'));
      }
    }

    onFetchChangeScreen();

    return Scaffold(
      backgroundColor: Hexcolor('#0c0166'),
      body: Center(
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            Center(
              child: Text(
                'SunDays',
                style: GoogleFonts.comfortaa(
                  color: Colors.white,
                  fontSize: 42,
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.all(16),
              child: Center(
                child: CircularProgressIndicator(
                  backgroundColor: Hexcolor('#fdbf00'),
                  valueColor:
                      AlwaysStoppedAnimation<Color>(Hexcolor('#ff8856')),
                  value: 0.3,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
