import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';

import '../component/current_weather.dart';
import '../component/data_failure.dart';
import '../data/failure_status.dart' as failure_info;
import '../component/today_sunlight.dart';
import '../component/week_sunlight.dart';

class SunOverView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Hexcolor('#0c0166'),
      appBar: AppBar(
        title: Text('SunDays',
            style: GoogleFonts.comfortaa(
              color: Colors.white,
              fontSize: 30,
            )),
        centerTitle: false,
        backgroundColor: Hexcolor('#0c0166'),
        iconTheme: new IconThemeData(color: Colors.white),
        actions: <Widget>[
          IconButton(
              icon: Tab(
                icon: Image.asset('assets/images/info.png'),
              ),
              onPressed: () {
                FlutterToast(context).showToast(
                    child: Text(
                  'This is an alpha app, soon stable features will be released',
                  style: TextStyle(color: Colors.white),
                ));
              })
        ],
      ),
      body: (failure_info.hasFailed)
          ? DataFailure()
          : ListView(
              children: <Widget>[
                Container(
                  height: (MediaQuery.of(context).size.height) * (1 / 3.6),
                  child: CurrentWeather(),
                ),
                Container(
                  child: Text("Today",
                      style: GoogleFonts.nunito(
                          fontSize: 20, color: Colors.white)),
                  height: (MediaQuery.of(context).size.height) * (1 / 13),
                  padding: EdgeInsets.only(
                    top: 20,
                    left: 16,
                    right: 16,
                  ),
                ),
                Container(
                  height: (MediaQuery.of(context).size.height) * (1 / 4.5),
                  padding: EdgeInsets.symmetric(
                    horizontal: 16,
                  ),
                  child: TodaySunlight(),
                ),
                Container(
                  child: Text("This Week",
                      style: GoogleFonts.nunito(
                          fontSize: 20, color: Colors.white)),
                  height: (MediaQuery.of(context).size.height) * (1 / 13),
                  padding: EdgeInsets.only(
                    top: 20,
                    left: 16,
                    right: 16,
                  ),
                ),
                Container(
                  height: (MediaQuery.of(context).size.height) * (1 / 5),
                  padding: EdgeInsets.symmetric(
                    horizontal: 16,
                  ),
                  child: WeekSunlight(),
                )
              ],
            ),
    );
  }
}
