import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';

import '../data/weather_report.dart' as weather_report;
import '../modules/predict_sunlight.dart';

class CurrentWeather extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Hexcolor('#0c0166'),
      body: Container(
        color: Hexcolor('#0c0166'),
        padding: EdgeInsets.all(16.0),
        width: MediaQuery.of(context).size.width,
        child: Center(
          child: ListView(
            shrinkWrap: true,
            children: <Widget>[
              Center(
                  child: Text('Nearest Sunny Day',
                      style: TextStyle(color: Colors.white))),
              Container(
                padding: EdgeInsets.all(16),
                child: Center(
                  child: Text(
                    findSunnyDayInWeek(),
                    style: GoogleFonts.nunito(
                      color: Colors.white,
                      fontSize: 24,
                    ),
                  ),
                ),
              ),
              Center(
                child: Text(
                  weather_report.store.timezone,
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
