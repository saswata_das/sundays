import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:hexcolor/hexcolor.dart';

import '../modules/extract_today_sunlight.dart' as today_sunlight;

class TodaySunlight extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(color: Colors.white),
          borderRadius: BorderRadius.circular(5),
          color: Hexcolor('#0c0166'),
        ),
        child: LineChart(LineChartData(
          lineTouchData: LineTouchData(enabled: false),
          lineBarsData: [
            LineChartBarData(
              spots: today_sunlight.todaySunArray,
              isCurved: true,
              colors: [
                const Color(0xfffdbf00).withOpacity(1),
                const Color(0xffff8856).withOpacity(1),
              ],
              barWidth: 5,
              dotData: FlDotData(
                show: false,
              ),
              belowBarData: BarAreaData(
                show: true,
                colors: [
                  const Color(0xfffdbf00).withOpacity(1),
                  const Color(0xffff8856).withOpacity(1),
                ],
              ),
            ),
          ],
          titlesData: FlTitlesData(
            leftTitles: SideTitles(showTitles: false),
            bottomTitles: SideTitles(
              textStyle: TextStyle(color: Colors.white),
              showTitles: true,
              getTitles: (value) {
                switch (value.toInt()) {
                  case 1:
                    return today_sunlight.fromTime;
                  case 6:
                    return today_sunlight.toTime;
                }
                return '';
              },
            ),
          ),
          axisTitleData: FlAxisTitleData(
            rightTitle: AxisTitle(showTitle: false),
            leftTitle: AxisTitle(showTitle: false),
            topTitle: AxisTitle(showTitle: false),
          ),
          gridData: FlGridData(show: false),
          borderData: FlBorderData(
            show: false,
          ),
          maxY: 1,
          minY: 0,
          maxX: 7,
          minX: 0,
        )),
      ),
    );
  }
}
