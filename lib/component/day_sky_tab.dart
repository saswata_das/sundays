import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

import '../modules/extract_week_weather.dart';

class DaySkyTab extends StatelessWidget {
  int index;

  DaySkyTab(int index) {
    this.index = index;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Hexcolor('#0c0166'),
      body: Container(
        margin: EdgeInsets.symmetric(horizontal: 4),
        child: Center(
          child: ListView(
            shrinkWrap: true,
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(vertical: 8),
                child: Center(
                  child:
                      Image(image: AssetImage(getWeatherIconForIndex(index))),
                ),
              ),
              Center(
                child: Text(
                  getDayNameForIndex(index),
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
