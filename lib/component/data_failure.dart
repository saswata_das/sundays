import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

import '../data/failure_status.dart' as failure_info;

class DataFailure extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final failureReason = failure_info.failureReason;

    void refreshScreen() {
      failure_info.resetFailure();
      Navigator.pushReplacementNamed(context, '/');
    }

    return Scaffold(
      backgroundColor: Hexcolor('#0c0166'),
      body: Center(
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            Center(
                child: Text('Oops failed to fetch data.',
                    style: TextStyle(color: Colors.white))),
            Center(
                child: Text('$failureReason',
                    style: TextStyle(color: Colors.white))),
            Center(
              child: OutlineButton(
                child: Text("Refresh", style: TextStyle(color: Colors.white)),
                onPressed: () => refreshScreen(),
                borderSide: BorderSide(
                  color: Colors.white,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30.0),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
