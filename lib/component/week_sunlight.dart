import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

import '../component/day_sky_tab.dart';

class WeekSunlight extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(color: Colors.white),
          borderRadius: BorderRadius.circular(5),
          color: Hexcolor('#0c0166'),
        ),
        child: Center(
          child: ListView.builder(
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemCount: 7,
              itemBuilder: (BuildContext listContext, int index) {
                return Container(
                  width: (MediaQuery.of(context).size.width) * (1 / 8),
                  child: DaySkyTab(index),
                );
              }),
        ),
      ),
    );
  }
}
