import 'package:http/http.dart' as http;

import '../data/weather_report.dart' as weather_report;
import '../data/last_report.dart';
import '../data/user_location.dart';
import '../modules/check_internet.dart';
import '../data/failure_status.dart' as failure_info;
import '../modules/extract_today_sunlight.dart';

const weatherBaseUrl =
    'https://api.darksky.net/forecast/575b00066400fd6a81a81dab62f32a3f';

Future<bool> fetchData() async {
  if (await checkInternetStatus()) {
    if (await userLocationReady()) {
      String weatherLocationUrl =
          '$weatherBaseUrl/$userLatitude,$userLongitude';
      if (await isWeatherRefreshRequired()) {
        try {
          var response = await http.get(weatherLocationUrl);
          weather_report.store = weather_report.overviewFromJson(response.body);
          setNewReport(response.body);
          setSunArray();
          return true;
        } catch (err) {
          throw err;
        }
      } else {
        weather_report.store =
            weather_report.overviewFromJson(await getLastReport());
        setSunArray();
        return true;
      }
    } else {
      failure_info.hasFailed = true;
      failure_info.failureReason = "Location not available";
      return true; //
    }
  } else {
    failure_info.hasFailed = true;
    failure_info.failureReason = "Internet not available";
    return true; //
  }
}
