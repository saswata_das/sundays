import 'package:fl_chart/fl_chart.dart';
import 'package:intl/intl.dart';

import '../data/weather_report.dart' as weather_report;

List<FlSpot> todaySunArray = [];
String fromTime = '0';
String toTime = '12';

void setSunArray() {
  if (weather_report.store != null) {
    _setEachDataForTime(0, weather_report.store.hourly.data);

    _setTimeRangeNames(
        _convertEpochToLocal(weather_report.store.hourly.data[1].time).toInt(),
        _convertEpochToLocal(weather_report.store.hourly.data[6].time).toInt());
  }
}

void _setTimeRangeNames(int from, int to) {
  if (from >= 0 && from < 12) {
    fromTime = "$from AM";
  } else if (from >= 12 && from <= 24) {
    from = from - 12;
    fromTime = "$from PM";
  }

  if (to >= 0 && to < 12) {
    toTime = "$to AM";
  } else if (to >= 12 && to <= 24) {
    to = to - 12;
    toTime = "$to PM";
  }
}

void _setEachDataForTime(int count, final list) {
  if (count < 8) {
    var dataNow = list[count];
    double time = _convertEpochToLocal(dataNow.time);
    double cloud = _getSunlightValue(dataNow.cloudCover, time);
    todaySunArray.add(FlSpot(count.toDouble(), cloud));
    _setEachDataForTime(count + 1, list);
  }
}

double _convertEpochToLocal(int epoch) {
  DateTime date = DateTime.fromMillisecondsSinceEpoch(epoch * 1000);
  var tFormat = DateFormat("H");
  return double.parse(tFormat.format(date));
}

double _getSunlightValue(double cloudCover, double time) {
  if (time > 6 && time < 17) {
    return 1 - cloudCover;
  } else {
    return 0;
  }
}
