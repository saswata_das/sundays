import 'package:intl/intl.dart';

import '../data/weather_report.dart' as weather_report;

String getDayNameForIndex(int index) {
  return _convertEpochToDay(weather_report.store.daily.data[index].time);
}

String getWeatherIconForIndex(int index) {
  return 'assets/images/' +
      weather_report.store.daily.data[index].icon +
      '.png';
}

String _convertEpochToDay(int epoch) {
  DateTime date = DateTime.fromMillisecondsSinceEpoch(epoch * 1000);
  var dFormat = DateFormat('E');
  return dFormat.format(date);
}
