import '../data/weather_report.dart' as weather_report;

String findSunnyDayInWeek() {
  int _closestDay = _checkInList(1, weather_report.store.daily.data);
  switch (_closestDay) {
    case 1:
      {
        return "Tomorrow";
      }
      break;
    case -1:
      {
        return "It's a sad sad week";
      }
      break;
    default:
      {
        return "$_closestDay days to go";
      }
      break;
  }
}

int _checkInList(int count, final queryList) {
  if (count == queryList.length - 1) {
    return -1;
  } else if (queryList[count].precipProbability < 0.3) {
    return count;
  } else {
    return _checkInList(count + 1, queryList);
  }
}
