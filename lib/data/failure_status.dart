bool hasFailed = false;
String failureReason;

void resetFailure() {
  hasFailed = false;
  failureReason = '';
}
