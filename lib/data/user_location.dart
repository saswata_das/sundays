import 'package:location/location.dart' as LocationStatus;
import 'package:geolocator/geolocator.dart';
import 'package:shared_preferences/shared_preferences.dart';

double userLongitude;
double userLatitude;

Future<bool> userLocationReady() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  userLatitude = (prefs.getDouble('LastLatitude') ?? -1);
  userLongitude = (prefs.getDouble('LastLongitude') ?? -1);
  if (userLatitude < 0 || userLongitude < 0) {
    return await updateLocation();
  } else {
    return true;
  }
}

Future<bool> updateLocation() async {
  if (await _isLocationAvailble()) {
    try {
      Geolocator geolocator = Geolocator()..forceAndroidLocationManager = true;

      Position position = await geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high);
      if (position != null) {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        await prefs.setDouble('LastLatitude', position.latitude);
        await prefs.setDouble('LastLongitude', position.longitude);

        userLatitude = position.latitude;
        userLongitude = position.longitude;

        return true;
      } else {
        return false;
      }
    } catch (err) {
      throw (err);
    }
  } else {
    return false;
  }
}

Future<bool> _isLocationAvailble() async {
  var location = new LocationStatus.Location();
  if (await location.serviceEnabled()) {
    return true;
  } else {
    return await location.requestService();
  }
}
