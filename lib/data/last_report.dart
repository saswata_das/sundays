import 'package:shared_preferences/shared_preferences.dart';

Future<bool> isWeatherRefreshRequired() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  int lastTime = (prefs.getInt('LastWeatherRefreshTime') ?? 0);
  if ((new DateTime.now().millisecondsSinceEpoch - lastTime) > 86400) {
    return true;
  } else {
    return false;
  }
}

Future<String> getLastReport() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return (prefs.getString('LastWeatherReport') ?? '');
}

void setNewReport(String report) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.setInt(
      'LastWeatherRefreshTime', new DateTime.now().millisecondsSinceEpoch);
  await prefs.setString('LastWeatherReport', report);
}
