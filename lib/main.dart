import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

import 'screen/sun_overview.dart';
import 'screen/app_splash.dart';

void main() {
  runApp(MaterialApp(
    title: 'SunDay Screens',
    theme: ThemeData(
      appBarTheme: AppBarTheme(
        elevation: 0,
      ),
      primaryColor: Hexcolor('#0c0166'),
    ),
    initialRoute: '/',
    routes: {
      '/': (context) => AppSplash(),
      '/overview': (context) => SunOverView(),
    },
  ));
}
