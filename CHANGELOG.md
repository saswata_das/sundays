# Change Log

All notable changes will be logged here

## [0.8.5 Released]

### [0.1.1] - 14-07-2020
#### Added 
- Init sun overview screen
- Add header
- Add header title and icons

#### Changed

### [0.1.2] - 14-07-2020
#### Added
- Add coming soon alert

#### Changed

### [0.2.1] - 14-07-2020
#### Added
- Add http call to weather forecast api
- Init splash screen
- Add splash timer

#### Changed

### [0.2.2] - 15-07-2020
#### Added
- Add weather data store

#### Changed

### [0.2.3] - 15-07-2020
#### Added
- Add weather report fetch restriction

#### Changed

### [0.3.1] - 15-07-2020
#### Added
- Add current weather component

#### Changed

### [0.3.2] - 15-07-2020
#### Added
- Add predict sunglight module

#### Changed

### [0.3.3] - 15-07-2020
#### Added

#### Changed
- Predict sunny days for day 1 to 7 in a week to avoid today

### [0.4.1] - 15-07-2020
#### Added
- Add get user location on first install

#### Changed
- Fix last report store

### [0.4.2] - 15-07-2020
#### Added
- Add location enable check
- Add ask location enable

#### Changed

### [0.5.1] - 16-07-2020
#### Added
- Add no internet or location component

#### Changed
- Fix location check

### [0.5.2] - 16-07-2020
#### Added
- Add failure reason

#### Changed

### [0.6.1] - 16-07-2020
#### Added
- Today sunlight graph's
- Filling sunlight graph data

#### Changed

### [0.7.1] - 16-07-2020
#### Added

#### Changed
- Change current weather from stateful to stateless

### [0.7.2] - 16-07-2020
#### Added
- Add week sunlight component
- Add day sky condition tab

#### Changed

### [0.7.3] - 16-07-2020
#### Added
- Add and load image assets

#### Changed

### [0.8.1] - 18-07-2020
#### Added

#### Changed
- Change header items

### [0.8.2] - 18-07-2020
#### Added

#### Changed
- Set to today sunlight and week sunglight as card

### [0.8.3] - 18-07-2020
#### Added
- Add text designs
- Add color themes

#### Changed
- Equalise section heights

### [0.8.4] - 18-07-2020
#### Added
- Add theme to splash

#### Changed

### [0.8.5] - 18-07-2020
#### Added
- Add theme
- Add app logo

#### Changed
- Fix fail screen ui
