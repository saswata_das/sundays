# sundays

An app to show and notify:

- Next sunny day in the week
- Today's sunlight graph
- Week's weather graph
- Remind before rain
- Remind before sunlight

The app mainly helps people to know:
- When to wash their clothes to dry on the right timming
- When to charge their solar cells
- Automated window open/close manipulation (In future)

_And for all the nocturnal creatures and vampires who wants to avoid the sunlight_